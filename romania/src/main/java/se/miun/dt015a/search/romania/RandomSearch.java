package se.miun.dt015a.search.romania;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import se.miun.dt015a.search.Successor;
import se.miun.dt015a.search.Solution;
import se.miun.dt015a.search.SearchProblem;
import se.miun.dt015a.search.SearchAlgorithm;

/**
 * A silly example search algorithm.
 *
 * @author Christoffer Fink
 */
public class RandomSearch<S,A> implements SearchAlgorithm<S,A> {

  private final Random rng = new Random();

  @Override
  public Solution<S,A> solve(SearchProblem<S,A> problem) {

    S state = problem.getInitialState();
    List<Successor<S,A>> path = new ArrayList<>();
    path.add(Successor.zeroth(state));

    while (!problem.isGoal(state)) {
      Successor<S,A> successor = pickRandomSuccessor(problem, state);
      path.add(successor);
      state = successor.getState();
    }
    return Solution.of(path);
  }

  private Successor<S,A> pickRandomSuccessor(SearchProblem<S,A> p, S s) {
    List<Successor<S,A>> successors = new ArrayList<>(p.getSuccessors(s));
    return successors.get(rng.nextInt(successors.size()));
  }
}
