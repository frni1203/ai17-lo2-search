package se.miun.dt015a.search.romania;

import se.miun.dt015a.search.*;

/**
 * An implementation of the A* algorithm.
 * Complete the solve method.
 *
 * @author Christoffer Fink
 */
public class Astar<S,A> implements InformedSearchAlgorithm<S, A> {

  @Override
  public Solution<S, A> solve(SearchProblem<S, A> problem, Heuristic<S> heuristic) {
    // Silly example.
    return new RandomSearch<S, A>()
      .solve(problem); // Your code here.
  }

  // For experimentation.
  public static void main(String[] args) {
    City start = City.LUGOJ;
    City goal = City.ARAD;
    TrProblem problem = TrProblem.getInstance(start, goal);

    City origin = problem.getInitialState();
    System.out.println("Starting in the city of " + origin);
    System.out.println("From there I can get to:");
    for (Successor<City,DriveAction> successor : problem.getSuccessors(origin)) {
      System.out.println("  " + successor.getState());
    }

    Astar<City, DriveAction> astar = new Astar<>();
    System.out.println("Starting search...");
    Solution<City,DriveAction> solution = astar.solve(problem, null);
    System.out.println("Found a solution with total cost " + solution.getCost());
    System.out.println("Solution visits these cities: " + solution.getStates());
  }
}
