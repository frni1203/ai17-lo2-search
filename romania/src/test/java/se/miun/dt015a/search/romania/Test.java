package se.miun.dt015a.search.romania;

import se.miun.dt015a.search.Heuristic;
import se.miun.dt015a.search.InformedSearchAlgorithm;
import se.miun.dt015a.search.SearchAlgorithm;
import se.miun.dt015a.search.util.TestVerbosity;

/**
 * Tests your implementation of A*.
 *
 * @author Christoffer Fink
 */
public class Test extends RomaniaTest {

  @Override
  public SearchAlgorithm<City, DriveAction> getAlgorithmInstance(Heuristic<City> h) {
    return InformedSearchAlgorithm.curry(new Astar<City, DriveAction>(), h);
  }

  @Override
  public TestVerbosity getVerbosity() {
    return TestVerbosity.BINARY;
    //return TestVerbosity.HINTS;
    //return TestVerbosity.FULL;
  }
}
