package se.miun.dt015a.search.river;

import java.util.concurrent.TimeUnit;

import org.junit.Rule;
import org.junit.rules.DisableOnDebug;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;

import se.miun.dt015a.search.SearchAlgorithm;
import se.miun.dt015a.search.SearchProblem;
import se.miun.dt015a.search.util.TestVerbosity;

/**
 * Tests your problem definition and algorithm for solving the problem.
 * The test requires you to find the shortest solution. It will also check
 * that your algorithm is reasonably efficient.
 *
 * @author Christoffer Fink
 */
public class Test extends RiverTest {
	
	@Rule
	public TestRule timeout = new DisableOnDebug(new Timeout(100, TimeUnit.DAYS));

  @Override
  public SearchProblem<RcState, RcAction> getProblemInstance() {
	  return new Problem();
  }

  @Override
  public SearchAlgorithm<RcState, RcAction> getAlgorithmInstance() {
	  return new Algorithm();
  }

  @Override
  public TestVerbosity getVerbosity() {
    return TestVerbosity.BINARY;
    //return TestVerbosity.HINTS;
    //return TestVerbosity.FULL;
  }
}
