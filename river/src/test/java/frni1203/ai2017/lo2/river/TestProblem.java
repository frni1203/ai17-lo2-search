package frni1203.ai2017.lo2.river;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.containsInAnyOrder;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import frni1203.ai2017.lo2.river.Action.ActionType;
import se.miun.dt015a.search.river.Problem;
import se.miun.dt015a.search.river.RcAction;
import se.miun.dt015a.search.river.RcState;
import se.miun.dt015a.search.river.State;

public class TestProblem {

	Problem problem;

	@Before
	public void setUp() {
		problem = new Problem();
	}

	@Test
	public void testCCMMML() {
		// Can only send cannibals or C+M or missionaries get outnumbered
		State state = new State(3, 3, true);
		Collection<RcAction> actions = problem.getPossibleActions(state);
		assertEquals(3, actions.size());
		Action c = Action.getActionInstance(ActionType.C);
		Action cc = Action.getActionInstance(ActionType.CC);
		Action cm = Action.getActionInstance(ActionType.CM);
		assertThat(actions, containsInAnyOrder(c, cc, cm));

		RcState resState = problem.getResultingState(state, c);
		assert (resState.boatRight());
		assertEquals(3, resState.missionariesLeft());
		assertEquals(2, resState.cannibalsLeft());
		assertEquals(0, resState.missionariesRight());
		assertEquals(1, resState.cannibalsRight());

		resState = problem.getResultingState(state, cc);
		assert (resState.boatRight());
		assertEquals(3, resState.missionariesLeft());
		assertEquals(1, resState.cannibalsLeft());
		assertEquals(0, resState.missionariesRight());
		assertEquals(2, resState.cannibalsRight());

		resState = problem.getResultingState(state, cm);
		assert (resState.boatRight());
		assertEquals(2, resState.missionariesLeft());
		assertEquals(2, resState.cannibalsLeft());
		assertEquals(1, resState.missionariesRight());
		assertEquals(1, resState.cannibalsRight());
	}

	@Test
	public void testCMMML() {
		// Could send missionaries over to get them killed
		State state = new State(3, 1, true);
		Collection<RcAction> actions = problem.getPossibleActions(state);
		assertEquals(2, actions.size());
		assertThat(actions,
				containsInAnyOrder(Action.getActionInstance(ActionType.C), Action.getActionInstance(ActionType.MM)));
	}

	@Test
	public void testCCMMR() {
		// Could send over cannibal to get missionaries on other side killed
		State state = new State(2, 2, false);
		Collection<RcAction> actions = problem.getPossibleActions(state);
		assertEquals(2, actions.size());
		Action m = Action.getActionInstance(ActionType.M);
		Action cm = Action.getActionInstance(ActionType.CM);
		assertThat(actions, containsInAnyOrder(m, cm));

		RcState resState = problem.getResultingState(state, m);
		assert (resState.boatLeft());
		assertEquals(3, resState.missionariesLeft());
		assertEquals(2, resState.cannibalsLeft());
		assertEquals(0, resState.missionariesRight());
		assertEquals(1, resState.cannibalsRight());

		resState = problem.getResultingState(state, cm);
		assert (resState.boatLeft());
		assertEquals(3, resState.missionariesLeft());
		assertEquals(3, resState.cannibalsLeft());
		assertEquals(0, resState.missionariesRight());
		assertEquals(0, resState.cannibalsRight());
	}

	@Test
	public void testCCMMMR() {
		// Test only one cannibal can be sent over, since no missionaries exist
		// on that side of the beach
		State state = new State(3, 2, false);
		Collection<RcAction> actions = problem.getPossibleActions(state);
		assertEquals(1, actions.size());
		assertThat(actions, containsInAnyOrder(Action.getActionInstance(ActionType.C)));
	}

	@Test
	public void testCCMML() {
		// Can send over only one cannibal without getting missionaries on other
		// side killed, or both missionaries to prevent the remaining
		// one killed
		State state = new State(2, 2, true);
		Collection<RcAction> actions = problem.getPossibleActions(state);
		assertEquals(2, actions.size());
		assertThat(actions,
				containsInAnyOrder(Action.getActionInstance(ActionType.MM), Action.getActionInstance(ActionType.CM)));
	}

	@Test
	public void testCL() {
		State state = new State(0, 1, true);
		Collection<RcAction> actions = problem.getPossibleActions(state);
		assertEquals(1, actions.size());
		Action c = Action.getActionInstance(ActionType.C);
		assertThat(actions, containsInAnyOrder(c));

		RcState resState = problem.getResultingState(state, c);
		assert (resState.boatRight());
		assertEquals(0, resState.missionariesLeft());
		assertEquals(0, resState.cannibalsLeft());
		assertEquals(3, resState.missionariesRight());
		assertEquals(3, resState.cannibalsRight());
	}

	@Test
	public void testGoalState() {
		State state = new State(0, 0, false);
		assert(problem.isGoal(state));
	}
}
