package frni1203.ai2017.lo2.river;


import static org.junit.Assert.*;

import org.junit.Test;

import frni1203.ai2017.lo2.river.Action.ActionType;


public class TestAction {

	@Test
	public void testInitialize() {
		Action c = Action.getActionInstance(ActionType.C);
		assertNotNull(c);
		assertEquals(ActionType.C, c.getActionType());
	}

	@Test
	public void testActionsAreSingletons() {
		Action c = Action.getActionInstance(ActionType.C);
		Action m = Action.getActionInstance(ActionType.M);
		Action c2 = Action.getActionInstance(ActionType.C);
		assertSame(c, c2);
		assertNotSame(c, m);
	}

}
