package frni1203.ai2017.lo2.river;


import java.util.HashMap;

import se.miun.dt015a.search.river.RcAction;

public class Action implements RcAction {
	/* Static members */
	public static enum ActionType {
		MM, M, C, CC, CM
	};

	private static HashMap<ActionType, Action> actionInstances = new HashMap<>();
	private static boolean initialized = false;

	public static Action getActionInstance(ActionType type) {
		if (!initialized) {
			Action.initialize();
		}
		return actionInstances.get(type);
	}

	private static void initialize() {
		actionInstances.put(ActionType.MM, new Action(ActionType.MM));
		actionInstances.put(ActionType.M, new Action(ActionType.M));
		actionInstances.put(ActionType.C, new Action(ActionType.C));
		actionInstances.put(ActionType.CC, new Action(ActionType.CC));
		actionInstances.put(ActionType.CM, new Action(ActionType.CM));

		initialized = true;
	}

	/* Class members */
	private final ActionType actionType;

	private Action(ActionType actionType) {
		this.actionType = actionType;
	}

	public ActionType getActionType() {
		return actionType;
	}

}
