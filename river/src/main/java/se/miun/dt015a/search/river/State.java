package se.miun.dt015a.search.river;

/**
 * An implementation of the River-Crossing state. Don't forget to also create an
 * implementation of RcAction.
 *
 * @author Christoffer Fink
 */
public class State implements RcState {
	// 1. Add any fields you deem appropriate for representing the state.
	// 2. Modify the methods below so that they return accurate information
	// about the state.
	// 3. You may add constructors (and additional methods) as necessary.

	private final int missionaries = 3;
	private final int cannibals = 3;
	private int missionariesLeft;
	private int cannibalsLeft;
	private boolean boatLeft;

	public State(int missionariesLeft, int cannibalsLeft, boolean boatLeft) {
		this.missionariesLeft = missionariesLeft;
		this.cannibalsLeft = cannibalsLeft;
		this.boatLeft = boatLeft;
		int missionariesRight = missionaries - missionariesLeft;
		int cannibalsRight = cannibals - cannibalsLeft;
		if ((this.missionariesLeft + this.cannibalsLeft == 0 && boatLeft)
				|| (missionariesRight + cannibalsRight == 0 && !boatLeft)) {
			throw new IllegalStateException("No one can access the boat");
		}
		if (killsMissionaries(missionariesLeft, cannibalsLeft, missionariesRight, cannibalsRight))
			throw new IllegalStateException("More cannibals than missionaries on one side, yum!");
	}

	public static boolean killsMissionaries(int missionaries1, int cannibals1, int missionaries2, int cannibals2) {
		if (cannibals1 > missionaries1 && missionaries1 != 0)
			return true;
		if (cannibals2 > missionaries2 && missionaries2 != 0)
			return true;
		return false;
	}

	@Override
	public int missionariesLeft() {
		return missionariesLeft;
	}

	@Override
	public int missionariesRight() {
		return missionaries - missionariesLeft;
	}

	@Override
	public int cannibalsLeft() {
		return cannibalsLeft;
	}

	@Override
	public int cannibalsRight() {
		return cannibals - cannibalsLeft;
	}

	@Override
	public boolean boatLeft() {
		return boatLeft;
	}

	@Override
	public boolean boatRight() {
		return !boatLeft;
	}
	
	public class Action implements RcAction {
		String name;

		public Action(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	// Some nice conveniences. You're welcome. ;)
	// You may change these if you really want to.

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof RcState)) {
			return false;
		}
		RcState other = (RcState) o;
		return missionariesLeft() == other.missionariesLeft() && missionariesRight() == other.missionariesRight()
				&& cannibalsLeft() == other.cannibalsLeft() && cannibalsRight() == other.cannibalsRight()
				&& boatLeft() == other.boatLeft() && boatRight() == other.boatRight();
	}

	@Override
	public int hashCode() {
		return missionariesLeft() * missionariesRight();
	}

	@Override
	public String toString() {
		int ml = missionariesLeft();
		int mr = missionariesRight();
		int cl = cannibalsLeft();
		int cr = cannibalsRight();
		int bl = boatLeft() ? 1 : 0;
		int br = boatRight() ? 1 : 0;
		return String.format("(%d,%d,%d,%d,%d,%d)", ml, cl, bl, br, mr, cr);
	}

}
