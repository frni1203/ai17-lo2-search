package se.miun.dt015a.search.river;

import se.miun.dt015a.search.SearchProblem;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import se.miun.dt015a.search.SearchAlgorithm;
import se.miun.dt015a.search.Solution;
import se.miun.dt015a.search.Successor;

/**
 * An implementation of an algorithm that solves the River-Crossing. Implement
 * an appropriate algorithm by filling out the solve method.
 *
 * @author Christoffer Fink
 */
public class Algorithm implements SearchAlgorithm<RcState, RcAction> {
	private final int maxDepth = 20;

	// You must choose an appropriate algorithm.
	// Feel free to add any helper classes you might need during the search.
	// What's important is that you can produce a Solution.
	// The solution is a list of Successor objects, starting with the Successor
	// holding the initial state.
	@Override
	public Solution<RcState, RcAction> solve(SearchProblem<RcState, RcAction> problem) {
		Solution<RcState, RcAction> solution = null;
		for (int depth = 1; solution == null && depth <= maxDepth; depth++) {
			solution = solveToDepth(problem, depth);
		}
		return solution;
	}

	private Solution<RcState, RcAction> solveToDepth(SearchProblem<RcState, RcAction> problem, int depth) {
		HashSet<State> explored = new HashSet<>();
		LinkedList<SuccessorNode<Successor<RcState, RcAction>>> frontier = new LinkedList<>();
		frontier.push(
				new SuccessorNode<Successor<RcState, RcAction>>(null, Successor.zeroth(problem.getInitialState()), 0));

		while (!frontier.isEmpty()) {
			SuccessorNode<Successor<RcState, RcAction>> thisNode = frontier.pop();
			State state = (State) thisNode.get().getState();
			if (problem.isGoal(state)) {
				Solution<RcState, RcAction> solution = Solution.of(makeSolutionList(thisNode));
				for(RcState s : solution.getStates()) {
					System.out.println(s);

				}
				return solution;
			}
			if(thisNode.getDepth() >= maxDepth) {
				//Don't explore any of its children
				continue;
			}
			explored.add((State) state);
			// Expand this successor, and add its children to the front
			Collection<Successor<RcState, RcAction>> childSuccessors = problem.getSuccessors(state);
			for (Successor<RcState, RcAction> childSuccessor : childSuccessors) {
				if (!explored.contains((State) childSuccessor.getState())) {
					// Add to the frontier
					frontier.push(new SuccessorNode<Successor<RcState, RcAction>>(thisNode, childSuccessor, thisNode.getDepth() + 1));
				}
			}
		}
		return null;
	}
	
	private <T> List<T> makeSolutionList(SuccessorNode<T> terminalNode) {
		LinkedList<T> solution = new LinkedList<>();
		SuccessorNode<T> predecessor = terminalNode;
		do {
			solution.push(predecessor.get());
		} while((predecessor = predecessor.getPredecessor()) != null);
		return solution;
	}

	private final class SuccessorNode<T> {
		private final SuccessorNode<T> predecessor;
		private final T nodeSuccessor;
		private final int depth;

		public SuccessorNode(SuccessorNode<T> predecessor, T successor, int depth) {
			this.predecessor = predecessor;
			this.nodeSuccessor = successor;
			this.depth = depth;
		}

		public SuccessorNode<T> getPredecessor() {
			return this.predecessor;
		}

		public T get() {
			return nodeSuccessor;
		}
		
		public int getDepth() {
			return depth;
		}

	}
}
