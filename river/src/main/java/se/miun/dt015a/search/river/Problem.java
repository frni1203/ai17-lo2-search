package se.miun.dt015a.search.river;

import java.util.Collection;
import java.util.HashSet;

import frni1203.ai2017.lo2.river.Action;
import frni1203.ai2017.lo2.river.Action.ActionType;
import se.miun.dt015a.search.util.AbstractSearchProblem;

/**
 * The definition of the River-Crossing problem. Your algorithm will use this
 * definition when solving the problem.
 *
 * @author Christoffer Fink
 */
public class Problem extends AbstractSearchProblem<RcState, RcAction> {

	public Problem() {
		super(new State(3, 3, true)); // Pass in the initial state here (instead
										// of null).
	}

	// Checks whether the given state satisfies the goal test.
	// Have we reached the goal?
	@Override
	public boolean isGoal(RcState state) {
		return state.cannibalsLeft() == 0 && state.missionariesLeft() == 0;
	}

	// Taking an action in some state results in a new state.
	// Given the current state and an action, return the new state.
	@Override
	public RcState getResultingState(RcState state, RcAction action) {
		// assume that the action is valid for the given state
		int missionariesLeft = state.missionariesLeft();
		// int missionariesRight = state.missionariesRight();
		int cannibalsLeft = state.cannibalsLeft();
		// int cannibalsRight = state.cannibalsRight();
		int direction = state.boatLeft() ? 1 : -1; // From right to left is
													// positive direction

		if (action == Action.getActionInstance(ActionType.CC)) {
			cannibalsLeft -= (2 * direction);
			// cannibalsRight += (2 * direction);
		} else if (action == Action.getActionInstance(ActionType.C)) {
			cannibalsLeft -= (1 * direction);
			// cannibalsRight += (1 * direction);
		} else if (action == Action.getActionInstance(ActionType.MM)) {
			missionariesLeft -= (2 * direction);
			// missionariesRight += (2 * direction);
		} else if (action == Action.getActionInstance(ActionType.M)) {
			missionariesLeft -= (1 * direction);
			// missionariesRight += (1 * direction);
		} else if (action == Action.getActionInstance(ActionType.CM)) {
			missionariesLeft -= (1 * direction);
			// missionariesRight += (1 * direction);
			cannibalsLeft -= (1 * direction);
			// cannibalsRight += (1 * direction);
		}
		return new State(missionariesLeft, cannibalsLeft, !state.boatLeft());
	}

	// The set of actions that can be taken in the given state.
	@Override
	public Collection<RcAction> getPossibleActions(RcState state) {
		HashSet<RcAction> actions = new HashSet<>();
		int missionariesFrom;
		int missionariesTo;
		int cannibalsFrom;
		int cannibalsTo;
		if (state.boatLeft()) {
			missionariesFrom = state.missionariesLeft();
			missionariesTo = state.missionariesRight();
			cannibalsFrom = state.cannibalsLeft();
			cannibalsTo = state.cannibalsRight();
		} else {
			missionariesFrom = state.missionariesRight();
			missionariesTo = state.missionariesLeft();
			cannibalsFrom = state.cannibalsRight();
			cannibalsTo = state.cannibalsLeft();
		}
		// Move two cannibals?
		if (cannibalsFrom >= 2
				&& !State.killsMissionaries(missionariesFrom, cannibalsFrom - 2, missionariesTo, cannibalsTo + 2))
			actions.add(Action.getActionInstance(ActionType.CC));

		// Move one cannibal?
		if (cannibalsFrom >= 1
				&& !State.killsMissionaries(missionariesFrom, cannibalsFrom - 1, missionariesTo, cannibalsTo + 1))
			actions.add(Action.getActionInstance(ActionType.C));

		// Move two missionaries?
		if (missionariesFrom >= 2
				&& !State.killsMissionaries(missionariesFrom - 2, cannibalsFrom, missionariesTo + 2, cannibalsTo))
			actions.add(Action.getActionInstance(ActionType.MM));

		// Move one missionary?
		if (missionariesFrom >= 1
				&& !State.killsMissionaries(missionariesFrom - 1, cannibalsFrom, missionariesTo + 1, cannibalsTo))
			actions.add(Action.getActionInstance(ActionType.M));

		// Move one of each?
		if (missionariesFrom >= 1 && cannibalsFrom >= 1 && !State.killsMissionaries(missionariesFrom - 1,
				cannibalsFrom - 1, missionariesTo + 1, cannibalsTo + 1))
			actions.add(Action.getActionInstance(ActionType.CM));

		return actions;
	}
}
